# What type of database do you want the site to run on?  postgres/mysql/sqlite
dbtype = sqlite

# What name does the database have? (If SQLite, enter the full filename)
dbname = /var/lib/openguides/sqlite/default.db

# ...the database user that can access that database?
dbuser = not-used

# ...the password that they use to access the database?
dbpass = not-used

# ...the machine that the database is hosted on? (blank if local)
dbhost = not-used

# ...the port the database is listening on? (blank if default)
dbport = not-used

# What do you want the script to be called?
script_name = wiki.cgi

# what directory contains the standard templates?
template_path = /usr/share/openguides/templates

# where should I look for custom templates?
custom_template_path = /etc/openguides/default/templates

# What URL does the install directory map to?
script_url = http://localhost/cgi-bin/openguides/

# There used to be a use_plucene configuration here, but currently,
# for the Debian package, this must be left at the default.

# What directory can I use to store indexes in for searching? ***NOTE*** This directory must exist and be writeable by the user that your script will run as.  See README for more on this.
indexing_directory = /var/lib/openguides/indexes/default/

# Do you want to enable page deletion?
enable_page_deletion = 0

# Please specify a password for the site admin:
admin_pass = Change this!

# What's the URL of the site's stylesheet? If you don't enter one here, the basic OpenGuides stylesheet will be used instead.
stylesheet_url = 

# What's the site called? (should be unique)
site_name = Default Debian Package OpenGuide

# Do you want the navigation bar included on the home page?
navbar_on_home_page = 1

# Do you want the ten most recent changes included on the home page?
recent_changes_on_home_page = 1

# Do you want the "Random Page" link to avoid returning a locale page?
random_page_omits_locales = 0

# Do you want the "Random Page" link to avoid returning a category page?
random_page_omits_categories = 0

# Do you want the content to appear above the navbar in the HTML?
content_above_navbar_in_html = 0

# What should the home page of the wiki be called?
home_name = Home

# How would you describe the site?
site_desc = A guide to [my city]

# What city is the site based in?
default_city = London

# What country is the site based in?
default_country = United Kingdom

# What language will the site be in? (Please give an ISO language code.)
default_language = en

# Contact email address for the site administrator?
contact_email = me@example.com

# What's the name of the node to use for the text formatting rules link?
formatting_rules_node = Text Formatting Examples

# Make node titles link to node backlinks (C2 style)?
backlinks_in_title = 0

# Distance calculation methods available are:  1) British National Grid  2) Irish National Grid  3) UTM ellipsoid. Which would you like to use?
geo_handler = 1

# Which ellipsoid do you want to use? (eg 'Airy', 'WGS-84')
# This is only used if you select geo_handler = 3 above
ellipsoid = WGS-84

# Do you want to use the Leaflet mapping library? (this is recommended)
use_leaflet = 1

# Do you have a Google Maps API key to use with this guide? If so, enter it here. (Note: our Google Maps support is deprecated, and we recommend you choose to use Leaflet instead.)
gmaps_api_key = 

# What is the longitude of the centre point of a map to draw for your guide? (This question can be ignored if you aren't using Google Maps - we recommend you use Leaflet instead, as our Leaflet code will figure this out for you.) You may paste in a Google Maps URL here (hint: copy URL from 'Link to this page')
centre_long = 0

# What is the latitude of the centre point of a map to draw for your guide? (This question can be ignored if you aren't using Google Maps - we recommend you use Leaflet instead, as our Leaflet code will figure this out for you.)
centre_lat = 0

# Would you like to display a map on every node that has geodata?
show_gmap_in_node_display = 1

# What default zoom level shall we use for Google Maps? (This question can be ignored if you aren't using Google Maps)
default_gmaps_zoom = 5

# What default zoom level shall we use for Google Maps in the search results? (This question can be ignored if you aren't using Google Maps)
default_gmaps_search_zoom = 3

# Forcibly treat stored lat/long data as if they used the WGS84 ellipsoid?
force_wgs84 = 0

# Do you have a Google Analytics key to use with this guide? If you enter it here, then Google Analytics functionality will be automatically enabled.
google_analytics_key =

# What licence will you use for the guide?
#licence_name = Creative Commons Attribution-ShareAlike 2.5

# What is the URL to your licence?
#licence_url = http://creativecommons.org/licenses/by-sa/2.5/

# What is the URL to your local page about your licensing policy?
#licence_info_url = http://www.example.com/

# Is the admin password required for moderating pages?
moderation_requires_password = 1

# Should nodes be allowed to have an externally hosted image?
enable_node_image = 1

# Do you want a common list of categories shown on all node pages?
enable_common_categories = 0

# Do you want a common list of locales shown on all node pages?
enable_common_locales = 0

# What module would you like to use for spam detection? (optional)
spam_detector_module =

# What directory should we install static content (CSS, images, javascript) to?
static_path = /usr/share/openguides/static

# What is the URL corresponding to the static content?
static_url = http://localhost/openguides-static

# Should we send email notifications when a moderated node is edited?
send_moderation_notifications = 1

# What module would you like to use to run an IP blacklist? (optional)
host_checker_module =

# Should the guide be read-only (no edits permitted)?
read_only = 0

# Should the site be mobile-friendly (responsive)?
responsive = 1
