Intro
-----

This is the Debian package of OpenGuides, a web application for running a
collaborative city guide. The project's web site is

<http://openguides.org/>

and contains information on how to join the network of guides and links to
existing guides, as well as more information on running the software.

Default configuration
---------------------

If the SQLite database library is installed, the package will attempt to
set up an SQLite database in the filesystem and install itself in the
default Debian location for CGI scripts. If you didn't have the SQLite
libraries (libdbd-sqlite-perl or libdbdsqlite3-perl) installed when you
installed the openguides package, but want to use SQLite now, you should run
"openguides-setup-db /etc/openguides/default/wiki.conf" as the web server
user after installing libdbd-sqlite3-perl (libdbd-sqlite-perl is now
deprecated and its on-disk format differs from libdbd-sqlite-perl, so is
not recommended for new installs).

To use this default configuration, point your web browser to

<http://localhost/cgi-bin/openguides/wiki.cgi>.

You will definitely want to edit the supplied configuration file at
/etc/openguides/default/wiki.conf to personalise your installation, and you
will need to install a CSS file to make the guide look nice. You may also
want to set up Apache mod_rewrite rules or tweak the custom templates;
all this is documented in /usr/share/doc/openguides/CUSTOMISATION
(for this Debian package, the custom templates for the default configuration
are in /etc/openguides/default/templates and there are example CSS files in
/usr/share/doc/openguides/examples/). Apache rewriting examples are available
in /usr/share/doc/openguides/examples/apache.openguides.conf.

Databases
---------

Although the default configuration installs using SQLite, you may wish to
use one of the two supported server-based databases, MySQL or PostgreSQL.
Once you have created the database and set appropriate access rights, you
should edit wiki.conf and set the configuration variables dbtype, dbname,
dbuser, dbpass, dbhost as appropriate. Then run the command:

  openguides-setup-db /etc/openguides/default/wiki.conf

as the web server user in order to initialise the database. Note that this
will not migrate the data from your previous database!

Multiple installs
-----------------

If you wish to run several guides on the same machine, you should create a
new CGI directory for that guide to live in, and set up symlinks to the four
CGI scripts in /usr/lib/cgi-bin/openguides. Create /etc/openguides/<yourguide>
and place a wiki.conf file there, and make a templates/ directory to put
your custom templates in. Update the custom_template_path to match this.
Finally, create a symlink in your new CGI directory pointing to
/etc/openguides/<yourguide>/wiki.conf so that the CGI scripts can find their
configuration.

Then run:
  openguides-setup-db /etc/openguides/<yourguide>/wiki.conf

as the web server user to initialize the new database.
